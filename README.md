## Description
Sample project how-to work with docker secrets using Java Spring


## Installation

- docker environment (Windows WSL should be enough)

## Usage

- compile the project

```
cd existing_repo
mvn install -DskipTests=true
```

- from docker define 2 secrets

```
printf "my-secret-password1-value" | docker secret create my-secret-password1 -
printf "my-secret-password2-value" | docker secret create my-secret-password2 -

```

- create a folder and define docker-compose.yml and copy the compiled jar (from target folder)

docker-compose.yml

```
version: "3.8"

services:

  java:
    image: openjdk:17-alpine
    volumes:
        - "./dockerSecretsPasswordProcessor-0.0.1-SNAPSHOT.jar:/app.jar"
    secrets:
       - my-secret-password1  
       - my-secret-password2
    command: ["java","-jar","/app.jar"]

secrets:
    my-secret-password1:
        external: true
    my-secret-password2:
        external: true        
        
```

- start 


```
docker stack deploy --compose-file=docker-compose.yml dockerSecretPasswordProcessor
```

- check the logs


```
docker logs dockerSecretPasswordProcessor_java.1.eztnu3rc5j90a8w2oyuq7y05v
```

- the result should have the values (my-secret-password-value1 and 2)

```
...
2022-06-23 06:57:27.587  INFO 1 --- [           main] r.f.d.objects.MainClass                  : -1-my-secret-password1-value
2022-06-23 06:57:27.587  INFO 1 --- [           main] r.f.d.objects.MainClass                  : -2-my-secret-password2-value
...
```

- to stop use

```
docker stack rm dockerSecretPasswordProcessor
```


## License
GPL