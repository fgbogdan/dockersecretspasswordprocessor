package ro.fagadar.dockerSecretsPasswordProcessor.objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MainClass {

    @Value("${my-secret-password1:not_defined_from_spring1}")
    private String mySecrePassword1;

    @Value("${my-secret-password2:not_defined_from_spring2}")
    private String mySecrePassword2;


    @Autowired
    public void init() {
        log.info("MainClass init called");
        log.info("-1-" + mySecrePassword1);
        log.info("-2-" + mySecrePassword2);
        
        try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
}