package ro.fagadar.dockerSecretsPasswordProcessor.exceptions;

public class DockerSecretException extends RuntimeException {
    public DockerSecretException(Exception e) {
        super(e);
    }
}
