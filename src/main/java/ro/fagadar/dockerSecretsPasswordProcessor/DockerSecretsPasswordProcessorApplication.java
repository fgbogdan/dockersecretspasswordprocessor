package ro.fagadar.dockerSecretsPasswordProcessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import ro.fagadar.dockerSecretsPasswordProcessor.objects.MainClass;

@SpringBootApplication
public class DockerSecretsPasswordProcessorApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext configurableApplicationContext = SpringApplication.run(DockerSecretsPasswordProcessorApplication.class, args);
		configurableApplicationContext.getBean(MainClass.class).init();
	}

}
