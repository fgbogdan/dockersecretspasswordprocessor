package ro.fagadar.dockerSecretsPasswordProcessor.processor;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.stream.Stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StreamUtils;

import lombok.extern.slf4j.Slf4j;
import ro.fagadar.dockerSecretsPasswordProcessor.exceptions.DockerSecretException;

@Slf4j
public class DockerSecretsPasswordProcessor implements EnvironmentPostProcessor {

	@Override
	public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {

		Path bindPath = Paths.get("/run/secrets");
		if (Files.isDirectory(bindPath)) {
			try (Stream<Path> dockerSecrets = Files.list(bindPath)) {
				dockerSecrets.forEach(p -> {
					String name = p.toFile().getPath();
					String propName = p.getFileName().toFile().getPath();
					Resource resource = new FileSystemResource(name);
					if (resource.exists()) {
						addEnvironmentVariable(resource, propName, environment);
					}
				});
			} catch (Exception e) {
				log.error("Error processing Docker Secrets", e);
			}
		}
	}

	private void addEnvironmentVariable(Resource resource, String propertyName, ConfigurableEnvironment environment) {
		try {
			String secretValue = StreamUtils.copyToString(resource.getInputStream(), Charset.defaultCharset());
			Properties props = new Properties();
			props.put(propertyName, secretValue);
			environment.getPropertySources().addLast(new PropertiesPropertySource(propertyName, props));
		} catch (IOException e) {
			throw new DockerSecretException(e);
		}

	}
}